import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class TestDonutShop {
	
	protected final static String CHROME_DRIVER_PATH = "C:\\chromedriver.exe";
	protected static WebDriver driver;
	
	
	@BeforeClass
	public static void setup() throws Exception {

		// Start Chrome
		System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_PATH);
		driver = new ChromeDriver();
		
		// Set the "waiting period" between each command
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	/** 
	 * Run this function AFTER all the test cases.
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDown() throws Exception {
		driver.quit();
	}
	
	@Test
	public void testDonutAdminPanel() throws InterruptedException {
		 //Home Page of the Admin panel
        String baseUrl = "http://localhost:8080/Donut/admin/";
        driver.get(baseUrl);
        Thread.sleep(3000);
        
        //Clicking on admin link
        WebElement admin = driver.findElement(By.className("admin"));
        admin.click();
        Thread.sleep(3000);
        
       //Show Products page
        WebElement show = driver.findElement(By.id("showproducts"));
        show.click();
        Thread.sleep(3000);
        
       //Entering details of the donut
       WebElement name = driver.findElement(By.id("productname"));
       name.sendKeys("Chocolate Donut");
       
       
       WebElement desc = driver.findElement(By.id("productdesc"));
       desc.sendKeys("Delicious donut");
       
       WebElement price = driver.findElement(By.id("productprice"));
       price.sendKeys("10");
       Thread.sleep(3000);
  
       WebElement adddonut = driver.findElement(By.id("addproduct"));
       adddonut.click();
       Thread.sleep(3000);
       
       WebElement goback = driver.findElement(By.id("goback"));
       goback.click();
       Thread.sleep(3000);
       
       //Home Page of the public panel
       String baseUrl1 = "http://localhost:8080/Donut/public/";
       driver.get(baseUrl1);
       Thread.sleep(3000);
       
       WebElement menu = driver.findElement(By.id("menu"));
       menu.click();
       Thread.sleep(3000);
       
       
	}


    
	
	
}
